const express = require("express");
const app = express();
const morgan = require("morgan");
const dotenv = require("dotenv");
const mongoose = require('mongoose');
const postRoutes = require("./routes/post");



dotenv.config();
app.use(morgan("dev"));

app.use("/", postRoutes);
const port = process.env.PORT || 8080;

mongoose.connect(process.env.MONGO_URI,{ useNewUrlParser: true } ).then((err)=>{

        app.listen(port, () => console.log(`listening on http://localhost:${port}`));

})

